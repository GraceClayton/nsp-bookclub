def diagonalArea():
    print("welcome to Diagonal Area Finder")
    d1 = input("What is diagonal 1?")
    d2 = input("What is Diagonal 2?")
    A1 = (int(d1)*int(d2))/2
    print("The area of the rhombus is ", A1)

# diagonalArea()

def sideArea():
    print("Welcome to Side Area finder")
    s1 = input("What is the length of side 1?")
    s2 = input("What is the length of side 2?")
    A2 = (int(s1)*int(s2))
    print("The area of the rhombus is ", A2)

# sideArea()

print("Would you like to find the diagonal area or the side area?")
ans = input()
if ans == "diagonal area" or "da":
    diagonalArea()
elif ans == "side area":
    sideArea()
else:
    print("please only input 'diagonal area' or 'side area'")